﻿using UnityEngine;
using System.Collections;

public class SceneGenerator : MonoBehaviour {

	// Use this for initialization


	public GameObject [] AvailableGroundArray;    // The first element must be the start ground;

	private Vector2 _nextAvailablePositon;
	private const float PLATFORMOFFSET =1.28f;
	private float _screenWidth;   				// Catch the screen width 


	void Start () 
	{

		// Get the start Y from the start ground;
		_nextAvailablePositon.y=AvailableGroundArray[0].transform.position.y;
		GameObject startGround = (GameObject)Instantiate(AvailableGroundArray[0]);
		// Get the start X from the camera size;
		_screenWidth = Camera.main.orthographicSize*Camera.main.aspect*2;
		_nextAvailablePositon.x = _screenWidth*0.5f;

	}


	void AddGround()
	{
		//Get a random Index
		int randomGroundIndex = Random.Range(0,AvailableGroundArray.Length);
		
		//create a new scene from random index
		GameObject Clonedground = (GameObject)Instantiate(AvailableGroundArray[randomGroundIndex]);
		
		//get the width of the new scene 
		float groundWidth = Clonedground.transform.FindChild("ground").localScale.x;
		_nextAvailablePositon.y = Clonedground.transform.position.y;
		//Debug.Log(Clonedground.transform.position.y);
		
		float groundCenter = _nextAvailablePositon.x+groundWidth*0.5f;
		//Debug.Log(nextAvailablePositon.x);
		
		//transform the new scene to the position
		
		//Generate the offset;
		//float offsetX = GetOffetX();
		float offsetX = 0;
		float offsetY = GetOffetY();
		
		Clonedground.transform.position = new Vector3(groundCenter+offsetX-0.1f,_nextAvailablePositon.y+offsetY,0);
		_nextAvailablePositon.x=_nextAvailablePositon.x+groundWidth+offsetX;
		//Debug.Log(groundWidth);
		//Debug.Log(ground.transform.position);
		

	}
	bool IsNeedAddGround()
	{
		
		float playerX = transform.position.x;
		//Debug.Log(screenWidth);
		
		if (playerX+_screenWidth> _nextAvailablePositon.x) 
		{
			return true;
		}
		
		return false;
	}
	void Update () 
	{

		if (IsNeedAddGround())
		{
			AddGround();	
		}
	}
	float GetOffetX()
	{
		float chance = 0;
		int  factor = 0;
		chance  = Random.Range(0f,1.0f);
		if (chance >0.8)
		{
			factor  = 4;
		}
		else if (chance >0.5) 
		{
			factor = 2;
			
		}
		else
		{
			factor = 0;
		}
		return factor*PLATFORMOFFSET;
	}
	float GetOffetY()
	{
		float chance = 0;
		float  factor = 0;
		chance  = Random.Range(0f,1.0f);
		if (chance >0.8)
		{
			factor  = 1f;
		}
		else if (chance >0.5) 
		{
			factor = 0.5f;
			
		}
		else
		{
			factor = 0f;
		}
		return factor*PLATFORMOFFSET;
	}
	
}


