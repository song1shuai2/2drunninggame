﻿using UnityEngine;
using System.Collections;

public class ObjectCleaner : MonoBehaviour {
	
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Cater") 
		{
			//Debug.Break();
			return;
		}

		//Debug.Log("trigger entered");

		if (other.gameObject.transform.parent) 
		{
			Destroy(other.gameObject.transform.parent.gameObject);
		}
		else
		{
			Destroy(other.gameObject);
		}

	
	}
}
