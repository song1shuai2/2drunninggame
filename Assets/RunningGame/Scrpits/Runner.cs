﻿using UnityEngine;
using System.Collections;

public class Runner : MonoBehaviour {

	// Use this for initialization

	private float distance;
	public Vector2 startPosition;
	public  float vSpeed;
	public  float hSpeed;
	public  float boostTime;

	Animator anim;

    bool grounded = false;
	public Transform groundCheck;
	float  groundRadius = 0.5f;
	public LayerMask whatIsGround;

	bool doubleJump = false;
	// player states;
	private bool isDie;
	private bool isBoost;

	private uint coins;
	//public  float runningDistance{get;};

	private BoxCollider2D aabb;

	//Runner Event and delegate
//	public delegate void PlayerDie();
//	public static event  PlayerDie OnPlayerDieEvent;
//
//	public delegate void PlayerBoost();
//	public static event  PlayerDie OnPlayerBoostEvent;


	void OnEnable()
	{
		GameEventManager.GameStart+=OnGameStart;
		GameEventManager.GameOver+=OnGameOver;

	}
	void OnDisable()
	{
//		GameEventManager.GameStart-=OnGameStart;
//		GameEventManager.GameOver-=OnGameOver;
	}
	void Start () 
	{
		//startPosition.x = transform.localPosition.x;

		isDie = false;
		anim = GetComponent<Animator>();
		aabb = gameObject.GetComponent<BoxCollider2D>();
		//runningDistance = 0f;
		//gameObject.SetActive(false);


	}

	 void OnGameStart()
	{
		distance = 0f;
		renderer.enabled = true;
		rigidbody2D.isKinematic =false;
		//Debug.Log("d");
		//enabled = true;

		gameObject.SetActive(true);
	}
	 void OnGameOver()
	{

		renderer.enabled = false;
		rigidbody2D.isKinematic = true;

	}

	
	// Update is called once per frame
	void Update () 
	{
		// update x speed
		grounded = Physics2D.OverlapCircle(groundCheck.position,groundRadius,whatIsGround);
		if((grounded||!doubleJump)&&Input.GetButtonDown("Jump"))
		{
			anim.SetBool("grounded",false);
			Vector2 veclocityY = rigidbody2D.velocity;
			veclocityY.y = vSpeed;
			rigidbody2D.velocity = veclocityY;
	

			if (!doubleJump &&!grounded) 
			{
				doubleJump = true;
			}
			
		}
		if(Input.GetAxis("Vertical")<0)
		{
			//Debug.Log("down");
			anim.SetBool("down",true);
			rigidbody2D.AddForce(new Vector2(0,-2));
		}
		else
		{
			anim.SetBool("down",false);
		}

		Vector2 boxSize = renderer.bounds.size;

	
     	aabb.size = boxSize;
		Vector2 center = renderer.bounds.center - transform.position;
		aabb.center = center;

	}

	void FixedUpdate()
	{
		if (!isDie) 
		{
			//Debug.Log(rigidbody2D.velocity.x);
			
			Vector2 veclocityX = rigidbody2D.velocity;
			veclocityX.x = hSpeed;
			rigidbody2D.velocity = veclocityX;
			//rigidbody2D.AddForce(new Vector2(hSpeed,0f));
		
			if (grounded) 
			{
				doubleJump = false;
			}
	    }
			anim.SetBool("grounded",grounded);
			anim.SetFloat("vSpeed",rigidbody2D.velocity.y);
	}
	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject.CompareTag("coins")) 
		{
			OnCollectCoin();
			Destroy(collider.gameObject);
		}
		if (collider.gameObject.CompareTag("boost")) 
		{
			hSpeed*=10;
			Invoke("BackToNormalSpeed",boostTime);
			Destroy(collider.gameObject);
//			if (OnPlayerBoostEvent!=null) 
//			{
//				OnPlayerBoostEvent();
//			}
		}

		if (collider.gameObject.CompareTag("dart")) 
		{

			isDie = true;
			anim.SetBool("dead",true);

			Debug.Log("i am die!!");
			GameEventManager.TriggerGameOver();


		}
		if (collider.gameObject.CompareTag("screenColision")) 
		{
			Debug.Log("i am die!!");
			GameEventManager.TriggerGameOver();
		}
	}
	void BackToNormalSpeed()
	{
		hSpeed *=0.1f;
	}

	void OnCollectCoin()
	{
		coins++;
	}
	public uint GetTheCountOfCoins()
	{
		return coins;
	}
	void OnBoost()
	{
		isBoost = true; 

	}



}
