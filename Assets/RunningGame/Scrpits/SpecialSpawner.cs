﻿using UnityEngine;
using System.Collections;

public class SpecialSpawner : MonoBehaviour {
	
		
		// Use this for initialization
		public float MinSpawnTime;
		public float MaxSpawnTime;
		
		public float StartWait;
		public GameObject Dart;
		public int Count;
		
		private float _screenHeight;
		void OnEnable()
		{
			//GameEventManager.GameStart+=LaunchDarts;
		}
		
		void Start ()
		{
			_screenHeight = Camera.main.orthographicSize*2f;
			
			StartCoroutine(LaunchDarts());
			
			
		}
		
		
		IEnumerator LaunchDarts ()
		{
			yield return new WaitForSeconds(StartWait);
			
			while(true)
			{
				for(int i = 0;i < Count;i++)
				{
					//Debug.Log("s");
					//float height = Random.Range(-_screenHeight*0.5f+1f,_screenHeight*0.5f-1f);
					GameObject clonedDart = (GameObject)Instantiate(Dart);
					clonedDart.transform.position = new Vector3(transform.position.x,transform.position.y,transform.position.z) ;
					yield return new WaitForSeconds(0.5f);
				}
				yield return new WaitForSeconds(Random.Range(MinSpawnTime,MaxSpawnTime));
			}
			
		}
}