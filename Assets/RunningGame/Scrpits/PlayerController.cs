﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	// Use this for initialization
	public float JumpForce; 
	public float RunSpeed;

	public AudioClip GetCoinSound;
	public AudioClip JumpSound;
	public AudioClip DoubleJumpSound;
	


	private bool _isjump;
	private bool _isDoubleJump;


	private bool _isOnGround;
	private Transform  _groundCheck;
	private Animator _anim;			 //Refecnce to the player's animtor components
	private BoxCollider2D _aabb;
	public float coinSpeed =2f;

	public float timeStamp;
	void Awake()
	{
		// set up reference 

		_isjump =false;
		_isDoubleJump = false;
		_groundCheck = transform.Find("GroundChecker");

		_anim = GetComponent<Animator>();
		_aabb = GetComponent<BoxCollider2D>();

	}
	// Update is called once per frame

	void Start()
	{

	}
	void Update () 
	{


		if (_isOnGround) 
		{
			_isDoubleJump = false;
		
		}

		// player can double jump only when player is on the gound or the double jump is true;
		if(Input.GetButtonDown("Jump")&&(_isOnGround||!_isDoubleJump))
		{

			_anim.SetBool("grounded",false);
			if (!_isDoubleJump&&!_isOnGround)
			{
				AudioSource.PlayClipAtPoint(DoubleJumpSound, transform.position);
				_isDoubleJump = true;
			}
			else
			{
				AudioSource.PlayClipAtPoint(JumpSound, transform.position);
			}


			rigidbody2D.AddForce(new Vector2(0,JumpForce));
		}
		// player down
		if(Input.GetAxis("Vertical")<0)
		{
			Debug.Log("down");
			_anim.SetBool("down",true);
			rigidbody2D.AddForce(new Vector2(0,-2));

		}
		else
		{
			_anim.SetBool("down",false);
		}

		ResetAABB();

	

	
	}
	void FixedUpdate()
	{

		_isOnGround = Physics2D.Linecast(transform.position,_groundCheck.position,1<<LayerMask.NameToLayer("Ground"));
		_anim.SetBool("grounded",_isOnGround);
		_anim.SetFloat("vSpeed",rigidbody2D.velocity.y);
		rigidbody2D.velocity = new Vector2( RunSpeed,rigidbody2D.velocity.y);

	}
	void ResetAABB()
	{
		Vector2 boxSize = renderer.bounds.size;
		Vector2 center = renderer.bounds.center - transform.position;
		_aabb.size = boxSize;
		_aabb.center = center;
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject.CompareTag("coins")) 
		{
	
			AudioSource.PlayClipAtPoint(GetCoinSound,collider.gameObject.transform.position);
			Destroy(collider.gameObject);
		} 

		if (collider.gameObject.CompareTag("dart")) 
		{
			
			//isDie = true;

			_anim.SetBool("dead",true);
			Debug.Log("i am die!!");


			
		}

	}
	
}
