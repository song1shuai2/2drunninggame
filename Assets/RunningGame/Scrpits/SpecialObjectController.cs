﻿using UnityEngine;
using System.Collections;

public class SpecialObjectController : MonoBehaviour {

	// Use this for initialization

	public AudioClip MagnetSound;
	public	float magnetForce = 1f;
	public	float magnetDistance = 5;
	public  float coinSpeed;
	public 	float magnetDuration;

	public float speed;


	void Start () 
	{

	}
	
	// Update is called once per frame

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject.CompareTag("Crate")) 
		{
		
			StartCoroutine(OnMagnet(collider.gameObject));

			gameObject.GetComponent<Renderer>().enabled = false;

			//gameObject.transform.parent = collider.gameObject.transform;
			speed = -3;


			Destroy(gameObject,magnetDuration);
		}
	}
	void Update()
	{
		transform.Translate(new Vector3(-speed*Time.deltaTime,Random.Range(-4f,4f)*Time.deltaTime,0));

	}

	IEnumerator OnMagnet(GameObject target)
	{
	
		
		//StartCoroutine(ApplyMagnet());
		while(true)
		{

			//AudioSource.PlayClipAtPoint(MagnetSound,target.gameObject.transform.position);
			Vector2 here = target.transform.position;
			
			int coinLayer  = LayerMask.NameToLayer("Coins");
			
			Collider2D [] coins = Physics2D.OverlapCircleAll(here,magnetDistance,1<<coinLayer);
			
			
			if (!audio.isPlaying)
			{
				audio.clip = MagnetSound;
				audio.Play();	
			}

			foreach (Collider2D coin in coins)
			{
				Vector2 coinPos = coin.transform.position;
				float dist = Vector2.Distance(here,coinPos);
				if (dist<magnetDistance) 
				{
					coin.transform.position = Vector2.MoveTowards(coinPos,here,Time.deltaTime*coinSpeed*magnetForce*(magnetDistance/dist));
				}
				
			}
			
			yield return null;
		}

	}
}
  