﻿using UnityEngine;
using System.Collections;

public class Timer : MonoBehaviour {

	// Use this for initialization
	private float startTime;
	public float expierTime;

	public void StartTimer()
	{
		startTime = Time.realtimeSinceStartup;

	}
	public bool IsExpiered()
	{

		float currentTime = Time.realtimeSinceStartup;

		if (currentTime - startTime >= expierTime*1000)
		{
			//Debug.Log("s");

			return true;
		}
		return false;
	}
	// Update is called once per frame

}
