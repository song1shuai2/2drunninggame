﻿using UnityEngine;
using System.Collections;

public class MainCameraController : MonoBehaviour {

	// Use this for initialization
	private GameObject _player;
	private float _speed;      //The camera move speed;

	public float smooth;
	private float  _relCameraPosX;
	private Vector2 _newPos;

	void Awake()
	{
		_player =  GameObject.FindGameObjectWithTag("Crate");

		// catch the camera position
		_relCameraPosX =  transform.position.x - _player.transform.position.x;

	}

	void FixedUpdate()
	{


		float newX = _player.transform.position.x+_relCameraPosX;
		transform.position = new Vector3( Mathf.Lerp(transform.position.x,newX,smooth*Time.deltaTime),transform.position.y,transform.position.z);


	}


}








