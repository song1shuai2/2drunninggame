using UnityEngine;
using System.Collections;
using AssemblyCSharp;

public class StateManager : MonoBehaviour
{

	private IStateBase _currentState;

	[HideInInspector]

	private static StateManager _instanceRef;
	 void Awake()
	{
		if (_instanceRef == null) 
		{
			_instanceRef = this;
			DontDestroyOnLoad(gameObject);

		}
		else
		{
			DestroyImmediate(gameObject);

		}
	}


		void Start ()
		{
	
			_currentState = new BeginState(this);

		}
	
		// Update is called once per frame
		void Update ()
		{
			if (_currentState!=null ) 
			{
				_currentState.StateUpdate();
				
			}
	
		}
		void OnGUI()
		{
			if (_currentState != null) 
			{
				_currentState.ShowIt();	 

			}
		}

		public void SwitchState(IStateBase newState)
		{
			_currentState  = newState;
		}
		
		public void Restart()
		{
			Destroy(gameObject);
			Application.LoadLevel("s");

		}
		
}

