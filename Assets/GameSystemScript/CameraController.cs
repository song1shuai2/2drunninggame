using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{

		// Use this for initialization
		private float worldDistanceFromCameraToTarget;
		public GameObject targetObject;
		public GameObject player;
		public bool isMove;
		private float speed;

		void OnEnable ()
		{
				GameEventManager.GameStart += OnGameStart;
				GameEventManager.GameOver += OnGameOver;
		}

		void Ondisable ()
		{
				GameEventManager.GameStart -= OnGameStart;
				GameEventManager.GameOver -= OnGameOver;

		}

		void Start ()
		{
				player = GameObject.FindGameObjectWithTag ("Player");
				speed = player.GetComponent<Runner> ().hSpeed;
				worldDistanceFromCameraToTarget = transform.position.x - targetObject.transform.position.x; 
				isMove = false;

		}
		// Update is called once per frame
		void Update ()
		{
				if (isMove) {
						//			float targetObjectX = targetObject.transform.position.x;
						//			Vector3 newPositionForCamera = transform.position;
						//			newPositionForCamera.x = targetObjectX+worldDistanceFromCameraToTarget;
						//			transform.position = newPositionForCamera;
						transform.Translate (new Vector3 (speed * Time.deltaTime, 0, 0));	
				}
	
		}

		void FixedUpdate ()
		{
	
		}

		void OnGameStart ()
		{
				isMove = true;
		
		}

		void OnGameOver ()
		{
				isMove = false;
		}

}
