using UnityEngine;
using System.Collections;

public class ParallaxBackground : MonoBehaviour {

	public GameObject []backgrounds;

	public float minSpeed;
	public float maxSpeed;

	private float increment;
	private bool  isPlayerMove;

	private GameObject player;
	private bool isMove;

	void OnEnable()
	{
		GameEventManager.GameStart+=OnGameStart;
		GameEventManager.GameOver+=OnGameOver;

	}
	void Start () 
	{
		 isMove = false;
		 player = GameObject.FindGameObjectWithTag("Player");


	}
	
	// Update is called once per frame
	void Update () 
	{

		maxSpeed = player.GetComponent<Runner>().hSpeed*0.1f;
		increment =maxSpeed/backgrounds.Length;
		//Debug.Log(maxSpeed);
		if (isMove) 
		{

			for (int i = 0; i < backgrounds.Length; i++) 
			{
				//Debug.Log(maxSpeed);
				backgrounds[i].renderer.material.mainTextureOffset=new Vector2(((increment*i)+minSpeed)*Time.time,0);
			}
		}
	
	}
	void OnGameStart()
	{
		isMove = true;
		Debug.Log("camera start");

	}
	void OnGameOver()
	{
		isMove = false;
	}

	void OnDisable()
	{
		GameEventManager.GameStart-=OnGameStart;
		GameEventManager.GameOver-=OnGameOver;
	}
}
