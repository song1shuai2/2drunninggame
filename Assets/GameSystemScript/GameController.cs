﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

	// Use this for initialization

	public GameObject  Player;
	public GameObject  Hud;
	public GameObject  camera ;
	public GameObject  parllaxBg;
	public GameObject  startGround;

	public delegate void GameEvent();
	public static event GameEvent GameStart,GameOver;
	
	 
	void OnEnable()
	{
		GameEventManager.GameStart+=OnGameStart;
	}

	void Start ()
	{

	}
	
	// Update is called once per frame
	void OnGUI()
	{
		UpdateHuds();
		//Hud.GetComponent<Hud>().
	
	}
	void UpdateHuds()
	{
		uint coinsCount = Player.GetComponent<Runner>(). GetTheCountOfCoins();

		Hud.GetComponent<Hud>().DisplayConinsCount(coinsCount);

	}
	void OnPlayerBoost ()
	{
	
		float speed = Player.GetComponent<Runner>().hSpeed;
		Debug.Log(speed);
		parllaxBg.GetComponent<ParallaxBackground>().maxSpeed = speed;
	
	}
	void Ondisable()
	{

	}

	public static void TriggerGameStart()
	{
		if (GameStart!= null) 
		{
			GameStart();
		}
	}
	void TriggerGameOver()
	{
		if (GameOver!=null)
		{
			GameOver();	
		}
	}
	void Update()
	{
		if (Input.GetButtonDown("Jump")) 
		{
			//Player.gameObject.SetActive(true);

		}

	}
	void OnGameStart()
	{
		GameObject start = (GameObject)Instantiate(startGround);


	}
	void OnDisable()
	{
		GameEventManager.GameStart-=OnGameStart;

	}

}
