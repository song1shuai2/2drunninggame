using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GroundSpawener : MonoBehaviour {

	// Use this for initialization
	private Vector2 nextAvailablePositon;

	public GameObject [] availableGroundArray;

	public GameObject [] NormalPickupObjects;
	public float NormalPickupChance;

	public GameObject [] SpecailPickupObjects;

	public float SpecailPickupChance;

	public float PickupMaxHeight;

	public Hashtable table;
	private float screenWidth; 
	private const float PLATFORMOFFSET =1.28f;


	
	void OnEnable()
	{
		GameEventManager.GameStart+=OnGameStart;
	}
	void OnGameStart()
	{
//		GameObject startScreen =GameObject.FindGameObjectWithTag("groundStart");
//		
//		nextAvailablePositon.y = startScreen.transform.position.y;
//		
//		screenWidth = Camera.main.orthographicSize*Camera.main.aspect*2;
//		nextAvailablePositon.x = screenWidth*0.5f;
	}

	void Start () 
	{
		GameObject startScreen =GameObject.FindGameObjectWithTag("groundStart");
		
		nextAvailablePositon.y = startScreen.transform.position.y;
		
		screenWidth = Camera.main.orthographicSize*Camera.main.aspect*2;
		nextAvailablePositon.x = screenWidth*0.5f;

//		AddGround();
//		Debug.Log(nextAvailablePositon);
//
//		AddGround();
//
//		GameEventManager.GameStart += OnGameStart;
	}

	bool IsNeedAddGround()
	{

		float playerX = transform.position.x;
		//Debug.Log(screenWidth);

		if (playerX+screenWidth> nextAvailablePositon.x) 
		{
			return true;
		}

		return false;
	}
	void AddGround()
	{
		//Get a random Index
		int randomGroundIndex = Random.Range(0,availableGroundArray.Length);
		
		//create a new scene from random index
		GameObject Clonedground = (GameObject)Instantiate(availableGroundArray[randomGroundIndex]);
		
		//get the width of the new scene 
		float groundWidth = Clonedground.transform.FindChild("ground").localScale.x;
		nextAvailablePositon.y = Clonedground.transform.position.y;
		//Debug.Log(Clonedground.transform.position.y);
		
		float groundCenter = nextAvailablePositon.x+groundWidth*0.5f;
		//Debug.Log(nextAvailablePositon.x);

		//transform the new scene to the position

		//Generate the offset;
		float offsetX = GetOffetX();
		float offsetY = GetOffetY();

		Clonedground.transform.position = new Vector3(groundCenter+offsetX-0.1f,nextAvailablePositon.y+offsetY,0);
		nextAvailablePositon.x=nextAvailablePositon.x+groundWidth+offsetX;
		//nextAvailablePositon.y=nextAvailablePositon.y+Clonedground.transform.FindChild("ground").localScale.x;
		//Debug.Log(groundWidth);
		//Debug.Log(ground.transform.position);


		/*Spawn pickups*/
//		float minHeight= Clonedground.transform.FindChild("surface").position.y + nextAvailablePositon.y;
//		float randomChance = Random.Range(0,1f);
//		float randomheight = Random.Range(minHeight,PickupMaxHeight);
//		if (randomChance < NormalPickupChance)
//		{
//			int randomIndex  =  Random.Range(0,NormalPickupObjects.Length);
//
//			Instantiate(NormalPickupObjects[randomIndex],new Vector3(groundCenter,randomheight,0),Quaternion.identity);
//
//		}
//		float randomChanceForSpecial = Random.Range(0,1f);
//		if (randomChanceForSpecial<SpecailPickupChance) 
//		{
//			int randomIndex  =  Random.Range(0,NormalPickupObjects.Length);
//			Instantiate(NormalPickupObjects[randomIndex],new Vector3(groundCenter,randomheight,0),Quaternion.identity);
//		}

	}

	void SpawnObjects()
	{
		//spawn normal object

	

	}


	
	// Update is called once per frame
	void Update () 
	{
		if (IsNeedAddGround())
		{
			AddGround();	
		}
	}
	float GetOffetX()
	{
		float chance = 0;
		int  factor = 0;
		chance  = Random.Range(0f,1.0f);
		if (chance >0.8)
		{
			factor  = 4;
		}
		else if (chance >0.5) 
		{
			factor = 2;
			
		}
		else
		{
			factor = 0;
		}
		return factor*PLATFORMOFFSET;
	}
	float GetOffetY()
	{
		float chance = 0;
		float  factor = 0;
		chance  = Random.Range(0f,1.0f);
		if (chance >0.8)
		{
			factor  = 1f;
		}
		else if (chance >0.5) 
		{
			factor = 0.5f;
			
		}
		else
		{
			factor = 0f;
		}
		return factor*PLATFORMOFFSET;
	}

}


//
//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//
//public class GroundSpawener : MonoBehaviour {
//	
//	public GameObject[] availableRooms;
//	
//	public List<GameObject> currentRooms;
//	
//	private float screenWidthInPoints;
//	
//	
//	// Use this for initialization
//	void Start () {
//		float height = 2.0f * Camera.main.orthographicSize;
//		screenWidthInPoints = height * Camera.main.aspect;
//	}
//	
//	// Update is called once per frame
//	void Update () {
//		
//	}
//	
//	void FixedUpdate () {
//		
//		GenerateRoomIfRequred();
//	}
//	
//	
//	void AddRoom(float farhtestRoomEndX)
//	{
//		//1
//		int randomRoomIndex = Random.Range(0, availableRooms.Length);
//		
//		//2
//		GameObject room = (GameObject)Instantiate(availableRooms[randomRoomIndex]);
//		
//		//3
//		float roomWidth = room.GetComponent<BoxCollider2D>().size.x;
//		
//		//4
//		float roomCenter = farhtestRoomEndX + roomWidth * 0.5f;
//		
//		//5
//		room.transform.position = new Vector3(roomCenter, 0, 0);
//		
//		//6
//		currentRooms.Add(room);			
//	} 
//	
//	void GenerateRoomIfRequred()
//	{
//		//1
//		List<GameObject> roomsToRemove = new List<GameObject>();
//		
//		//2
//		bool addRooms = true;        
//		
//		//3
//		float playerX = transform.position.x;
//		
//		//4
//		float removeRoomX = playerX - screenWidthInPoints;        
//		
//		//5
//		float addRoomX = playerX + screenWidthInPoints;
//		
//		//6
//		float farhtestRoomEndX = 0;
//		
//		foreach(var room in currentRooms)
//		{
//			//7
//			float roomWidth = room.GetComponent<BoxCollider2D>().size.x;
//			float roomStartX = room.transform.position.x - (roomWidth * 0.5f);    
//			float roomEndX = roomStartX + roomWidth;                            
//			
//			//8
//			if (roomStartX > addRoomX)
//				addRooms = false;
//			
//			//9
//			if (roomEndX < removeRoomX)
//				roomsToRemove.Add(room);
//			
//			//10
//			farhtestRoomEndX = Mathf.Max(farhtestRoomEndX, roomEndX);
//		}
//		
//		//11
//		foreach(var room in roomsToRemove)
//		{
//			currentRooms.Remove(room);
//			Destroy(room);            
//		}
//		
//		//12
//		if (addRooms)
//			AddRoom(farhtestRoomEndX);
//	}
//}

