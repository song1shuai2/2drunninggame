﻿using UnityEngine;
using System.Collections;

public class ObjectSpawner : MonoBehaviour {

	public GameObject [] NormalPickupObjects;
	public float NormalPickupChance;
	
	public GameObject [] SpecailPickupObjects;
	
	public float SpecailPickupChance;
 	
	void Start ()
	{


		float randomChance = Random.Range(0,1f);
		if (randomChance < NormalPickupChance)
		{
			int randomIndex  =  Random.Range(0,NormalPickupObjects.Length);
			Instantiate(NormalPickupObjects[randomIndex],transform.position,Quaternion.identity);
			
		}
		float randomChanceForSpecial = Random.Range(0,1f);
		if (randomChanceForSpecial<SpecailPickupChance) 
		{
			int randomIndex  =  Random.Range(0,NormalPickupObjects.Length);
			Instantiate(NormalPickupObjects[randomIndex],transform.position,Quaternion.identity);
		}
		
	}

	
	// Update is called once per frame

}