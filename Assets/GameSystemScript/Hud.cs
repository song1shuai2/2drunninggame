﻿using UnityEngine;
using System.Collections;

public class Hud : MonoBehaviour {

	 // Use this for initialization
	public Texture2D CoinIcon;	
	public GUIText gameOverText,instructionsText;
	private bool isStart;
	private bool isOver;
	
	public void DisplayConinsCount(uint coinCounts)
	{
		// show the 
		Rect coinIconRect = new Rect(10,10,32,32);
		GUI.DrawTexture(coinIconRect,CoinIcon);

		GUIStyle style = new GUIStyle();
		style.fontSize=30;
		style.fontStyle = FontStyle.Bold;
		style.normal.textColor = Color.yellow;

		//Debug.Log("get %d",)
		Rect labelRect = new Rect(coinIconRect.xMax,coinIconRect.y,60,32);
		GUI.Label(labelRect,coinCounts.ToString(),style );
	}
	void DisplayRestart()
	{
		Rect buttonRect = new Rect(Screen.width *0.35f,Screen.height*0.45f,Screen.width*0.30f,Screen.height*0.1f);
		if (GUI.Button(buttonRect,"Restart")) 
		{
			//Application.loadedLevel(Application.loadedLevelName);
		};
			
	}
	void DisplayGameStart()
	{
		Rect buttonRect = new Rect(Screen.width *0.35f,Screen.height*0.45f,Screen.width*0.30f,Screen.height*0.1f);
		if (GUI.Button(buttonRect,"Start")) 
		{
			GameEventManager.TriggerGameStart();
			isStart = true;

		};
	}

	 void Start()
	{
		gameOverText.enabled =false;
		GameEventManager.GameOver+=OnGameOver;
		GameEventManager.GameStart+=OnGameStart;
		
	}

	void Update()
	{
//		if (Input.GetButtonDown("Jump")) 
//		{
//			GameEventManager.TriggerGameStart();
//		}

	}
	void OnGameStart()
	{
		gameOverText.enabled = false;
		instructionsText.enabled =false;
		enabled = false;
		isStart = true;
		
	}
	void OnGameOver()
	{
		gameOverText.enabled = true;
		instructionsText.enabled = true;
		enabled = true;
		isStart = false;
	
	}
	void OnGUI()
	{
		if (!isStart) 
		{
			DisplayGameStart();	
		}

	}
	
}
